<!--- Remove sections that do not apply -->

This issue tracker is only for technical issues related to abacoin-core.

General abacoin questions and/or support requests and are best directed to the [abacointalk.io forums](https://abacointalk.io/).

For reporting security issues, please contact the Abacoin developers on the #abacoin-dev Freenode IRC channel or alternatively you can email us at contact@abacoin.org.

### Describe the issue

### Can you reliably reproduce the issue?
#### If so, please list the steps to reproduce below:
Please provide the command that led to the issue. Copy and paste the command line and the output into the issue and attach any files we'll need to reproduce the bug.
Screenshots and much harder to deal with because we cannot reren your commands or see the entire setup.

Whenever possible, provide the smallest amount of code needed to produce the bug dependably.  If you cannot reproduce the bug, we likely will not be able to either.

If you had a problem while trying to build aba from source, please include the output of 'opam list -i' and any error messages that you saw while building.
If you ran a second command which fixed the problem, provide us with the error you saw initially in addition to telling us how you fixed the bug.

### Expected behaviour
Tell us what should happen

### Actual behaviour
Tell us what happens instead

### Screenshots.
If the issue is related to the GUI, screenshots can be added to this issue via drag & drop.

### What version of abacoin-core are you using?
List the version number/commit ID, and if it is an official binary, self compiled or a distribution package such as PPA.

### Machine specs:
- OS:
- CPU:
- RAM:
- Disk size:
- Disk Type (HD/SDD):

### Any extra information that might be useful in the debugging process.
This is normally the contents of a `debug.log` or `config.log` file. Raw text or a link to a pastebin type site are preferred.
