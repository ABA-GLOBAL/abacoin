The Project

ABAG Coin is a distributed consensus platform with meta-consensus capability. 

ABAG Coin not only comes to consensus about the state of its ledger, like Bitcoin or Ethereum. 

It also attempts to come to consensus about how the protocol and the nodes should adapt and upgrade.

 • Developer documentation is available online at http://www.gitlab.com/ABA-GLOBAL/abagcoin always in sync with the master branch 

  (which may be desynchronized with the code running on the live networks, 

   replace master in the URL by the branch of your choice: mainnet, alphanet, zeronet, to make sure you are consulting the right API version)

 • The website http://www.abagcoin.org contains more information about the project.

 • All development happens on GitLab at http://www.gitlab.com/ABA-GLOBAL/abagcoin

The source code of Tezos is placed under the MIT Open Source License.


The Community

 • Several community built block explorers are available:
    
    ◦ http://explorer.abagcoin.org

 • A few community run websites collect useful ABAG Coin links:
    
    ◦ http://www.facebook.com/abaglobalall.co

    ◦ http://twitter.com/abaglobalall

    ◦ https://cafe.naver.com/abaglobalgroup

 • There is a matrix channel ABAG Coin that you can join here https://riot.im/app/#/room/!DnEkJNoEkUpSfMivnO:matrix.org

 • There is a sub-reddit at https://www.reddit.com/r/chat/comments/9gqcmc/abag/

 • There is a free talking about ABAG Coin for Korean https://talk.cafe.naver.com/channels/159051640592

 • There is a fan club of ABAG Coin in Korea 

    ◦ https://band.us/band/71233785

    ◦ https://band.us/band/71233785

    ◦ https://band.us/band/66149431

    ◦ https://band.us/band/68965764


 • There is also a community FAQ at 

    ◦ https://github.com/abaglobal/abacoin/tree/master/doc

    ◦ https://github.com/abaglobal/abacoin/readme

The Networks

The ABAG Coin Alpha (test) network has been live and open since January 2018.

The ABAG Coin Beta (experimental) network has been live and open since August 2018.
